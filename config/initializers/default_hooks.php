<?php

// This function include screen.css in wp_head() function

function enqueue_stylesheets() {
  //wp_register_style("screen", stylesheet_url("screen"), false, false);
  //wp_enqueue_style("screen");
}

add_action('wp_enqueue_scripts', 'enqueue_stylesheets');

// This function include jquery and application.js in wp_footer() function

function enqueue_javascripts() {
  //wp_enqueue_script("jquery");
  if (is_page('landing-page')) {
    wp_register_script("landing", javascript_url("landing"), '', false, true);
    wp_enqueue_script("landing");
  } else {
    wp_register_script("main", javascript_url("main"), '', false, true);
    wp_enqueue_script("main");
  }
}

add_action('wp_enqueue_scripts', 'enqueue_javascripts');
