<?php

/*
 * Enable WordPress menu support uncommenting the line below
 */
add_theme_support('menus');

function register_custom_menus() {
  /*
   * Place here all your register_nav_menu() calls.
   */

  register_nav_menus( array(
  	'mobile' => 'Mobile Navigation Menu',
  	'desktop' => 'Desktop Navigation Menu',
  	'footer' => 'Footer menu'
  ) );
}

add_action('init', 'register_custom_menus');

