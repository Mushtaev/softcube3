<?php

the_posts_pagination( array(
    'mid_size' => 2,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),
	) );

function wpc_register_wp_api_endpoints() {
	register_rest_route( 'softcube', '/request-demo', array(
        'methods' => 'POST',
        'callback' => 'wpc_softcube_request_demo_callback',
        'args' => array(
          'name' => array(
            'required' => true,
          ),
          'email' => array(
            'required' => true,
          ),
          'site' => array(
            'required' => true,
          ),
          'newsletter' => array(
            'required' => false,
          ),
          'subscribe' => array(
            'required' => false,
          ),
        ),
    ));
}

function wpc_softcube_request_demo_callback( $request_data ) {
  $parameters = $request_data->get_params();

  if(
    isset($parameters['name']) &&
    isset($parameters['email']) &&
    isset($parameters['site'])
  ) {
  
	$email = 'alex.mushtaev@softcube.com';
	$subject = 'Get a demo Softcube';
  if($parameters['newsletter']){
    $subject = 'Get newsletter';
  }
  if($parameters['subscribe']){
    $subject = 'Create your Softcube account';
  }

  $useremail = $parameters['email'];
  $name =  $parameters['name'];
  $page_url = $parameters['site'];

  /*$mail_headers="Content-type:multipart/form-data;charset=utf-8\r\n";
  $mail_headers .= "Cc: softcube.com < sales.not.spam@softcube.com >\n"; 
  $mail_headers .= "X-Sender: softcube.com < sales@softcube.com >\n";
  $mail_headers .= "X-Priority: 1\n";
  $mail_headers .= "Return-Path: alex.mushtaev@softcube.com\n";
  $message = 'Name: ' . $parameters['name'] . ' , Email: ' . $parameters['email'] . ' , Site: ' . $parameters['site'];
  $verify = mail($email, $subject, $message, $mail_headers);*/
    $message = 'Name: ' . $parameters['name'] . ' , Email: ' . $parameters['email'] . ' , Site: ' . $parameters['site'];

  $message_body = <<<EOM
	Email: $useremail \n
	Name: $name \n
	URL: $page_url \n
EOM;

	wp_mail('olga.zhyla@softcube.com',$subject,$message_body);
	wp_mail('alex.mushtaev@softcube.com',$subject,$message_body);
	wp_mail('ivan.khit@softcube.com',$subject,$message_body);
	wp_mail('julia.rudenko@softcube.com',$subject,$message_body);
	wp_mail('maria.t@softcube.com',$subject,$message_body);
	wp_mail('oleg.lesov@softcube.com',$subject,$message_body);


    /*try {
      wp_mail( $email, $subject, $message );
    } catch (Exception $e) {
      return array(error => true);
    }*/

    return array(ok => true);
  }

  return array(error => true);
}

add_action( 'rest_api_init', 'wpc_register_wp_api_endpoints' );
add_theme_support( 'post-thumbnails' );

/* register new image size
 * 200px wide and unlimited height
 */
add_image_size( 'height250', 99999, 250, false );
add_image_size( 'height575', 99999, 575, false );


function get_related_articles() {
  wp_reset_postdata();
		global $post;

		// Define shared post arguments
		$args = array(
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'ignore_sticky_posts'    => 1,
			'orderby'                => 'rand',
			'post__not_in'           => array( $post->ID ),
			'posts_per_page'         => 3,
		);
		/*$arrs = get_post_meta($post->ID);
		echo '$arrs' . $arrs . '<br>';
		foreach ($arrs as $arr) {
			foreach ($arr as $ar) {
				echo 'val' . $ar . '<br>';
			}
		};*/
		// Related by categories
		if ( get_theme_mod( 'colormag_related_posts', 'categories' ) == 'categories' ) {

			$cats = get_post_meta( $post->ID, 'related-posts', true );

			if ( ! $cats ) {
				$cats                 = wp_get_post_categories( $post->ID, array( 'fields' => 'ids' ) );
				$args['category__in'] = $cats;
			} else {
				$args['cat'] = $cats;
			}
		}
		// Related by tags
		if ( get_theme_mod( 'colormag_related_posts', 'categories' ) == 'post_tag' ) {

			$tags = get_post_meta( $post->ID, 'related-posts', true );

			if ( ! $tags ) {
				$tags            = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ) );
				$args['tag__in'] = $tags;
			} else {
				$args['tag_slug__in'] = explode( ',', $tags );
			}
			if ( ! $tags ) {
				$break = true;
			}
		}
		//echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$post_01 = get_post_meta(get_the_ID(), 'related_articles_id_01', true);
		$post_02 = get_post_meta(get_the_ID(), 'related_articles_id_02', true);
		$post_03 = get_post_meta(get_the_ID(), 'related_articles_id_03', true);


		if(!$post_01) {
			$query = ! isset( $break ) ? new WP_Query( $args ) : new WP_Query();
		} else {
			$query = new WP_Query( array('post__in' => array($post_01, $post_02, $post_03)));
		}
		return $query;
}

function true_wp_posts_nofollow_callback($matches) {
  $a = $matches[0];
  $site_url = site_url();

  if (strpos($a, 'rel') === false && strpos($a, 'softcube') === false){
      $a = preg_replace("%(href=\S(?!$site_url))%i", 'rel="nofollow" $1', $a);
  } elseif (preg_match("%href=\S(?!$site_url)%i", $a) && strpos($a, 'softcube') === false){
      $a = preg_replace('/rel=S(?!nofollow)\S*/i', 'rel="nofollow"', $a);
  }

  if (strpos($a, 'target') === false){
    $a = preg_replace("%(href=\S(?!$site_url))%i", 'target="_blank" $1', $a);
  } elseif (preg_match("%href=\S(?!$site_url)%i", $a)){
    $a = preg_replace('/target=S(?!_blank)\S*/i', 'target="_blank"', $a);
  }
  return $a;
}
 
function true_wp_posts_nofollow($content) {
  return preg_replace_callback('/<a[^>]+/', 'true_wp_posts_nofollow_callback', $content);
}
 
add_filter('the_content', 'true_wp_posts_nofollow');



function res_fromemail($email) { return 'noreply@softcube.com'; }
function res_fromname($email){ return 'SoftCube'; }
add_filter('wp_mail_from', 'res_fromemail');
add_filter('wp_mail_from_name', 'res_fromname');