<?php
global $wp_query;
render_view("static/search", "posts/search");
?>
<style>
	.search {
	width: calc(33% - 37.5px);
    margin-top: 20px;
	display: inline-block !important;
	margin-right: 3%;
	}
    .search a{width: 100%;}
	@media screen and (max-width: 728px) {
		.search {
		width: 100%;
		margin-top: 20px;
		display: block !important;
		}
	 }
	#searchform {
	padding: 0;
    margin: 0;
	}
	.post-preview__title.search{
	cursor: pointer;
    display: block;
    margin-bottom: 20px;
	}
	.aria-current, .page-numbers{
	font-size: 1rem;
    font-weight: bold;
    background: #f4c956;
    border-radius: 2em;
    padding-left: 1em;
    padding-right: 1em;
    line-height: 3em;
    display: -webkit-inline-box;
    color: #292a29;
    border: none;
    cursor: pointer;
    text-decoration: blink;
    margin-right: 0.5em;
	}
    .page-numbers {
        display: inline-block;
        width: 40px;
        height: 40px;
        background-color: #344046;
        font-weight: 700;
        text-align: center;
        opacity: .8;
        cursor: pointer;
        border-radius: 20px;
        color: #fff;
        line-height: 2.3;
        text-decoration: none;
        padding-left: 0;
        padding-right: 0;
    }
</style>
