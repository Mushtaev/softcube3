import tingle from 'tingle.js';

export default class Cookies {
    constructor() {

        function getCookie(name) {
            const nameEQ = name + "=";
            let ca = document.cookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function setCookie(name, value, days) {
            let expires = "";
            if (days) {
                let date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + (value || "") + expires + "; path=/";
        }

        if (getCookie("softcube") === "true") {
            document.querySelector("#bottombar").remove();
        }

        let clickEvent = (function () {
            if ('ontouchstart' in document.documentElement === true)
                return 'touchstart';
            else
                return 'click';
        })();

        if (document.querySelector("#bottombar")) {
            //document.querySelector("#bottombar").style.display = "flex";
            document.querySelector(".pb-button.pb-button--small.pb-button--primary").addEventListener(clickEvent, function () {
                setCookie("softcube", "true", 360);
                document.querySelector("#bottombar").remove();
            })
        }

        if (document.querySelector("#return-to-top")) {
            document.querySelector('.return-to-top').addEventListener(clickEvent, function () {
                let scrollToTop = window.setInterval(function () {
                    let pos = window.pageYOffset;
                    if (pos > 0) {
                        window.scrollTo(0, pos - 50); // how far to scroll on each step
                    } else {
                        window.clearInterval(scrollToTop);
                    }
                }, 6);
            });
        }
        if (window.location.href.indexOf("/tag/") > -1) {
            document.querySelector("head").insertAdjacentHTML("afterbegin", "<meta name='robots' content='noindex, follow'>");
        }
    }
}