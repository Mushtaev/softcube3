var clickEvent = (function() {
  if ('ontouchstart' in document.documentElement === true)
    return 'touchstart';
  else
    return 'click';
})();
function function_accordion() {
	document.querySelectorAll(".accordion__item").forEach(function(items){
		items.addEventListener(clickEvent, function(){
			if(this.classList.contains("accordion__item--closed")) {
				this.classList.remove("accordion__item--closed");
				this.classList.add("accordion__item--open");
			} else {
				this.classList.remove("accordion__item--open");
				this.classList.add("accordion__item--closed");
			}
		})
	})
}
function function_mobile_menu_open() {
	document.querySelectorAll(".header__hamburger")[0].addEventListener(clickEvent, function(){
		document.querySelector("body").insertAdjacentHTML('beforeend', document.querySelector("#mobile-menu").innerHTML);
		setTimeout(function() { 
			document.querySelectorAll(".hamburger")[0].classList.add("hamburger--visible"); 
			function_mobile_menu_close();
		}, 300);
	})
}
function function_mobile_menu_close() {
	document.querySelectorAll(".hamburger__close")[0].addEventListener(clickEvent, function(){
		document.querySelectorAll(".hamburger")[0].classList.remove("hamburger--visible");
		setTimeout(function() { 
			document.querySelectorAll(".hamburger")[0].remove(); 
		}, 700);
	})
}
function_accordion();
function_mobile_menu_open();

var el = document.querySelector('header');
window.onscroll = function() {
	var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled > 100) {
        el.classList.add('header--scrolling');
    } else {
    	el.classList.remove('header--scrolling');
    }
};
