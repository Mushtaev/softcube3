import tingle from 'tingle.js';

export default class Modal {
    constructor() {
        const modal = this.template = document.querySelector('#modal');

        if (!modal) {
            return;
        }

        this.onOpen = this.onOpen.bind(this);
        this.onClose = this.onClose.bind(this);
        const openLinks = document.querySelectorAll('.cards__button');
        this.template = modal.text;
        this.templateConfirmation = document.querySelector('#modal-confirmation').text;
        this.templateError = document.querySelector('#modal-error').text;
        this.modal = new tingle.modal({
            footer: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: null,
            cssClass: ['modal'],
            onOpen: this.onOpen,
            onClose: this.onClose
        });

        this.modal.setContent(this.template);

        Array.from(openLinks).forEach(link => {
            link.addEventListener('click', (e) => {
                e.stopPropagation();
                this.modal.open();
            });
        });
    }

    onOpen() {
        this.form = this.modal.getContent().querySelector('#lets_chat');
        this.form.addEventListener('.signup-form__submit', (e) => {
            e.preventDefault();

            const formData = [...this.form.elements]
                .reduce((acc, {name, value}) => {
                    if (name && value) {
                        acc.append(name, value);
                    }

                    return acc;
                }, new FormData());

            fetch('/wp-json/softcube/request-demo', {
                method: 'POST',
                body: formData,
            }).then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            }).then((response) => {
                if (response.error === true) {
                    this.modal.setContent(this.templateError);
                    this.bindCloseButton();
                } else {
                    ga('send', 'event', 'contact us', 'click', 'subscribe');
                    fbq('track', 'ContactUs');
                    qp('track', 'CompleteRegistration');
                    this.modal.setContent(this.templateConfirmation);
                    this.bindCloseButton();
                }
            }).catch(() => {
                this.modal.setContent(this.templateError);
                this.bindCloseButton();
            });
        });
    }

    onClose() {
        this.modal.setContent(this.template);
    }

    bindCloseButton() {
        const button = this.modal.getContent().querySelector('.modal__overlay');

        button.addEventListener('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            document.querySelectorAll(".modal")[0].remove();
        });
    }
}