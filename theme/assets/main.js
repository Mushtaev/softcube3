//import Header from './javascripts/header';
import Modal from './javascripts/modal';
import Cookies from './javascripts/cookies';


const main = () => {
  //new Header({ el: document.getElementById('header') });
    new Modal();
    new Cookies();
};

document.addEventListener('DOMContentLoaded', main, false);