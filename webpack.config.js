const path = require('path');

const srcDir = path.resolve(__dirname, 'theme/assets');
const dstDir = path.resolve(__dirname, 'assets');
const javascriptsDstPath = path.join(dstDir, '/javascripts');

const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        main: [
            'babel-core/register',
            'babel-polyfill',
            path.join(srcDir, '/main.js')
        ],
        landing: [
            'babel-core/register',
            'babel-polyfill',
            path.join(srcDir, '/landing.js')
        ]
    },
    mode: 'production',
    output: {
        filename: '[name].js',
        path: javascriptsDstPath,
    },

    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: [require('babel-plugin-transform-class-properties')],
                        presets: ['babel-preset-env']
                    }
                }
            },
            {
                test: /\.s(a|c)ss$/,
                loader: ExtractTextPlugin.extract('css-loader?sourceMap!resolve-url-loader!sass-loader?sourceMap')
            },
            {test: /\.css$/, loaders: ['style-loader', 'css-loader', 'resolve-url-loader']},
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    {
                        loader: 'file-loader',
                        options: {
                            name:'[hash].[ext]',
                            outputPath: '../images/',
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            gifsicle: {
                                interlaced: false,
                            },
                            optipng: {
                                optimizationLevel: 7,
                            },
                        }
                    },
                ]
            },
            {
                test: /\.(mp4|webm)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=[hash].[ext]'
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file-loader?name=../fonts/[name].[ext]'
            }
        ],
    },
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                cache: true,
                parallel: 4,
                uglifyOptions: {
                    ecma: 6,
                    output: {
                        comments: false,
                        beautify: false,
                    },
                }
            })
        ],
    },
    plugins: [
        new BrowserSyncPlugin({
            host: '127.0.0.1',
            port: 3000,
            proxy: {
                target: 'http://127.0.0.1:8080',
            },
        }),
        new ExtractTextPlugin('../stylesheets/screen.css'),
    ],
};
